# Alex Gun Dealer

Alex Gun Dealer is feature rich bot developed in Node.js & Discord.js for Discord Servers.

<a href>
    <img src="https://top.gg/api/widget/881612238841077861.svg">
</a>

**Overview**

Alex Gun Dealer Is A Discord Bot Developed In Node.js & Discord.js


**Permissions**


Alex Gun Dealer Need Several Permission To Do What It Is Supposed To Do.Every Permission Alex Gun Dealer Needs Is Explained Below..

* **View Channels** is Needed So Alex Can See Channels.
* **Send Messages** is Needed so Alex Can Send Messages In Channel.
* **Embed Links** is Needed So Alex Can Attach Embeds In Channel.
* **Read Message History** is Needed So Alex Can Read Message History In Channel.

[Invite Alex Gun Dealer](https://discord.com/api/oauth2/authorize?client_id=881612238841077861&permissions=84992&scope=bot)



<h2> Installation Guide </h2>
 
<h3>Before You Begin</h3>

1.Make Sure That You Have Installed Node.js(15.14.0 Or Higher)
And Git


2.Follow The Below Instructions

3.Clone Repository

4.Change Your Directory To Alex Gun Dealer with `cd alex-gun-dealer` After Cloning The Repository

5.Run `npm install` to Install The Necessary Dependencies

6.Edit The `example-config.json` And Fill The Bot Token. You Can Find Your Bot Token On Discord Developer Portal. You Can Put Your Customize Prefix Too. Just Edit The ``example-config.json`` To ``config.json``.

7.After That Run `npm start` Or `npm run start` To Start The Bot Once Bot Logs In You Will See It In Your Console.



**Contribution Guide**


Contribution to this project is not only limited to coding help, You can
suggest a feature, help with docs, design ideas or even some typos. You
are just an issue away.  Don't hesitate to create an issue.
❤️



Made In India With ❤️
