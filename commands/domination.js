exports.run = (client, message, args) => {
    const Discord = require('discord.js')
    const domination = new Discord.MessageEmbed()
        .setTitle("Game Mode: Domination")
        .setColor("RANDOM")
        .setDescription("Capture and hold flags across the map to earn points for your team.")
        .setTimestamp()
    return message.channel.send({embeds: [domination]});
}