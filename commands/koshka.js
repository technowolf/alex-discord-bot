const guns = require('../data/guns.json')
const Discord = require("discord.js");
exports.run = (client, message, args) => {
    const Discord = require('discord.js')
    const Koshka = guns.find (gun => gun.name === "koshka")
    const koshka = new Discord.MessageEmbed()
        .setTitle("Koshka Base Stats")
        .setColor("RANDOM")
        .setImage('https://i.imgur.com/N2yxVYn.jpeg')
        .setDescription(`Koshka has base damage of ${Koshka.baseDamage}, \n Koshka has base Accuracy of ${Koshka.baseAccuracy}, \n Koshka has base Firerate of ${Koshka.baseFirerate}`)
    return message.channel.send({embeds: [koshka]})
}