exports.run = (client, message, args) => {
    const Discord = require('discord.js')
    const frontline = new Discord.MessageEmbed()
        .setTitle("Game Mode: Frontline")
        .setColor("RANDOM")
        .setDescription("After spawning at a team base, kill players on the opposing team.")
        .setTimestamp()
    return message.channel.send({embeds: [frontline]});
}