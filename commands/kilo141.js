const guns = require('../data/guns.json')
exports.run = (client, message, args) => {
    const Discord = require('discord.js')
    const kilo = guns.find (gun => gun.name = "pkm")
    const kilo141 = new Discord.MessageEmbed()
        .setTitle("KILO 141 Base Stats")
        .setColor("RANDOM")
        .setDescription(`Kilo 141 has the base damage of ${kilo.baseDamage}, \n Kilo 141 has the base firerate of ${kilo.baseFirerate}, \n Kilo 141 has the base accuracy of ${kilo.baseAccuracy}`)
        .setImage('https://i.imgur.com/DXVoUhc.jpeg')
    return message.channel.send({embeds: [kilo141]})
}